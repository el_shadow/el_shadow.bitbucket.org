/**
 * Created by EL on 12.11.13.
 */
var converter=new Object();
converter.lib = new Object();
converter.lib.digit = {
    0: "ноль",
    1: "один",
    2: "два",
    3: "три",
    4: "четыре",
    5: "пять",
    6: "шесть",
    7: "семь",
    8: "восемь",
    9: "девять",
    10: "десять",
    11: "одиннадцать",
    12: "двенадцать",
    13: "тринадцать",
    14: "четырнадцать",
    15: "пятнадцать",
    16: "шестнадцать",
    17: "семнадцать",
    18: "восемнадцать",
    19: "девятнадцать",
    20: "двадцать",
    30: "тридцать",
    40: "сорок",
    50: "пятьдесят",
    60: "шестьдесят",
    70: "семьдесят",
    80: "восемьдесят",
    90: "девяносто",
    100: "сто",
    200: "двести",
    300: "триста",
    400: "четыреста",
    500: "пятьсот",
    600: "шестьсот",
    700: "семьсот",
    800: "восемьсот",
    900: "девятьсот",
    lvl1_1: "одна",
    lvl1_2: "две"
};
/*
 * level is int*10^(3*level)
 * */
converter.lib.level = {
    zero0: {
        name: "нол",
        1: "ь",
        2: "я",
        5: "ей"
    },
    x0: {
        name: "",
        1: "",
        2: "",
        5: ""
    },
    x1: {
        name: "тысяч",
        1: "a",
        2: "и",
        5: ""
    },
    x2: {
        name: "миллион",
        1: "",
        2: "а",
        5: "ов"
    },
    x3: {
        name: "миллиард",
        1: "",
        2: "а",
        5: "ов"
    },
    x4: {
        name: "триллион",
        1: "",
        2: "а",
        5: "ов"
    }
};
converter.lib.words={
    "%":" проценты ",
    "№":" номер ",
    "@":" собачка ",
    "пр.":" проспект ",
    "ул.":" улица ",
    "г.":" год ",
    "т.":" тонн ",
    "д.":" дом ",
    "кв.":" квартира ",
    "кг.":" килограмм ",
    "см.":" сантиметр ",
    "гр.":" грамм ",
    "$":" долларов ",
    "кВт":" киловатт ",
    "км/ч":" километров в час ",
    "авт.":" автор "
};
converter.lib.getDigitWords = function (int, level) {
    int = parseInt(int);
    var res = "";
    var str = "" + int;
    if (parseInt(str) > 99) {
        res += this.digit[str[0] * 100] + " ";
        str = str.substr(1, 2);
    }
    if (parseInt(str) > 19) {
        res += this.digit[str[0] * 10] + " ";
        if (str[1] != "0") {
            if (level == 1 && (parseInt(str[1]) == 1 || parseInt(str[1]) == 2)) {
                res += this.digit["lvl1_" + parseInt(str[1])] + " ";
            }
            else {
                res += this.digit[parseInt(str[1])] + " ";
            }
        }
    }
    else {
        if (parseInt(str) > 0) {
            if (level == 1 && (parseInt(str) == 1 || parseInt(str) == 2)) {
                res += this.digit["lvl1_" + parseInt(str)] + " ";
            }
            else {
                res += this.digit[parseInt(str)] + " ";
            }
        }
    }
    return res;
};
converter.lib.getLevel = function (l, s) {
    s = parseInt(s);
    var suf = s == 0 || s >= 5 ? 5 : s >= 2 ? 2 : 1;
    var res = this.level["x" + l].name + this.level["x" + l][suf];
    if (res.length > 0) {
        res += " ";
    }
    return res;
};

converter.convertZero=function(str){
    if (str == "") return str;
    var count="";
    if (str.length>1){
        count+=this.convert(""+str.length)+" ";
    }
    var z = str.length+"";
    z = z[z.length-1];
    var suf = z==0 || z >= 5 ? 5 : z >= 2 ? 2 : 1;
    var zz = this.lib.level["zero0"].name + this.lib.level["zero0"][suf];
    return count+zz;
};

converter.convert = function (str) {
    var res = "";
    if (str == "") return str;
    if (str >= 1000000000000000) return str;
    if (str == 0){
        return converter.convertZero(str);
    }
    var str2num = parseInt(str);
    str2num+="";
    var zeros="";
    if (str.length>str2num.length){
        for(var x=0;x<str.length;x++){
            if (str[x]==0){
                zeros+=str[x];
            }
            else break;
        }
        str=str.substr(zeros.length,str.length-zeros.length);
        zeros=converter.convertZero(zeros);
    }
    var level = 0;
    while (str.length >= 3) {
        var sub = str.substr(str.length - 3, 3);
        if (parseInt(sub) > 0) {
            res = this.lib.getDigitWords(sub, level) + this.lib.getLevel(level, sub[2]) + res;
        }
        str = str.substr(0, str.length - 3);
        level++;
    }
    if ((str.length > 0) && (parseInt(str) > 0)) {
        res = this.lib.getDigitWords(str, level) + this.lib.getLevel(level, str[str.length - 1]) + res;
    }
    if (typeof("".trim) == "function"){
        res=res.trim();
    }
    if (zeros!=""){
        res=zeros+" "+res;
    }
    return res;
};

converter.convertInts = function (str){
    var res = {
        text: "",
        html: ""
    };
    var tempInt = "";
    var okstart = true;
    var okend = false;
    for (var x = 0; x < str.length; x++) {
        if (/\d/.test(str[x]) && okstart) {
            tempInt += str[x];
        }
        else {
            if (tempInt.length > 0) {
                if (/\s/.test(str[x]) || /\D/.test(str[x])) {
                    okend = true;
                }
                else {
                    okend = false;
                    okstart = false;
                    res.text += tempInt;
                    res.html += tempInt;
                    tempInt = "";
                }
            }
            else okstart = (/\s/.test(str[x]));
            if (okstart && okend && tempInt.length > 0) {
                res.text += this.convert(tempInt);
                res.html += "<span class='digitConverted'>" + this.convert(tempInt) + "</span>";
                tempInt = "";
                okstart = true;
                okend = false;
            }
            res.text += str[x];
            res.html += str[x];
        }
    }
    if (okstart && tempInt.length > 0) {
        res.text += this.convert(tempInt);
        res.html += "<span class='digitConverted'>" + this.convert(tempInt) + "</span>";
    }
    return res;
};

var chronoCalc;
chronoCalc = {};
chronoCalc.data = {
    time: 0,
    symbols: 0,
    pages: 0,
    words: 0
};
chronoCalc.getMin=function(time){
    var res="";
    var mins = parseInt(time/60);
    if (mins>0){
        res+=mins+"&nbsp;мин ";
    }
    var sec=time-(mins*60);
    if (sec>0){
        res+=sec+"&nbsp;сек ";
    };
    return res;
};
chronoCalc.analize=function(str){
    this.data.symbols=str.replace(/ /g,"").length;
    var words=str.split(/\s+|-+|_+|\,+|\.+/);
    var wordscount=0;
    jQuery.each(words,function(){
        if (this!==undefined && this.length>2){
            wordscount++;
        }
    });
    this.data.words=wordscount;
    this.data.pages=(this.data.words/350).toFixed(2)+" стр.";
    var realwords=0;
    jQuery.each(words,function(index,value){
        if (value.length>2) realwords++;
    });
    var korr=0;
    var range=2;
    var koeff=0.5;
    if (realwords>120){
        range=Math.round(realwords*0.0334);
        koeff=0.6294;
    }
    else{
        korr=realwords==0?0:realwords<20?1:2;
    }
    this.data.time=Math.ceil(realwords*koeff)+korr;
    if (this.data.time>0){
        if (jQuery("#out-minutes").prop("checked") && (this.data.time+range>60)){
            this.data.time="&asymp; "+chronoCalc.getMin(this.data.time)+"&nbsp;&ndash; "+chronoCalc.getMin(this.data.time+range);
        }
        else{
            this.data.time="&asymp; "+this.data.time+"&nbsp;&ndash; "+(this.data.time+range)+"&nbsp;сек";
        }
    }
    return this.data;
};
chronoCalc.exclude = function (str, option) {
    switch (option) {
        case "latin":
            str = str.replace(/[A-Za-z]/g, "");
            break;
        case "brackets":
            str = str.replace(/\(.*?\)/g, "");
            break;
        case "digits":
            str = str.replace(/\d/g, "");
            break;
    }
    return str;
};
chronoCalc.escapeHtml=function(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;");
};
chronoCalc.update = function () {
    var $texarea = jQuery("#hrono-textarea");
    var result = chronoCalc.escapeHtml($texarea.val());
    var $divresult = jQuery("#hrono-result");
    if (jQuery("#ex-latin").prop("checked")) {
        result = this.exclude(result, "latin");
    }
    if (jQuery("#ex-brackets").prop("checked")) {
        result = this.exclude(result, "brackets");
    }
    if (jQuery("#ex-digits").prop("checked")) {
        result = this.exclude(result, "digits");
    }
    if (jQuery("#hrono-autoDigits").prop('checked') && !jQuery("#ex-digits").prop('checked')) {
        var conv = converter.convertInts(result);
        $divresult.html(conv.html);
        result = (conv.text);
    }
    else {
        $divresult.html(result);
    }
    var divhtmltext=$divresult.html();
    for (var key in converter.lib.words) {
        var pref="";
        if(!(/\%|\№|\@|\$/.test(key))){
            pref="\\s";
        }
        var keyReg= new RegExp(pref+key.replace(".","\\.").replace("$","\\$").replace("/","\\/"),"g");
        result=result.replace(keyReg,converter.lib.words[key]);
        divhtmltext=divhtmltext.replace(keyReg,"<span class='wordConverted'>"+converter.lib.words[key]+"</span>")
    }
    $divresult.html(divhtmltext);
    var res = this.analize(result);
    jQuery("#hrono-time").html(res.time);
    jQuery("#hrono-symbols").text(res.symbols);
    jQuery("#hrono-pages").text(res.pages);
    jQuery("#hrono-words").text(res.words);
    $divresult.html($divresult.html().replace(/\n/g, "<br>"));
};
